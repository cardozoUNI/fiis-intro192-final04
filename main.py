import random

def cabinas_disp():
  cabin = random.sample(range(1,11), random.randint(1, 10))
  cabin.sort()
  return cabin

print("Red de Bibliotecas")
user_name = input("Ingrese su nombre de usuario:\n> ")
passwrd = input("\nIngrese su clave:\n> ")
print("\nBienvenido",user_name)

while True:
  print("Por favor, eliga una opcion:")
  print("1. Prestamo de libros.")
  print("2. Alquiler de cabinas.")
  print("3. Separar Area audiovisual.")
  print("4. Seccion de reclamos.")
  opn = input("Opcion:")
  while opn != "1" and opn != "2" and opn != "3" and opn != "4":
    opn = input("Solo escriba '1', '2', '3' o '4'\n")
  if opn == "1":
    print("\nEliga una opcion: ")
    print("1. Busqueda de libros.")
    print("2. Seleccionar Area.")
    opn_1 = input("Opcion:")
    while opn_1 != "1" and opn_1 != "2":
      opn_1 = input("Por favor, solo escriba '1' o '2': ")
    if opn_1 == "1":
      input("Escriba nombre del libro: ") 
      print("Libro disponible.")
      print("Libro adquirido!")
    elif opn_1 == "2": 
      print("\nEliga un area: ")
      print("1. Area de historia, literatura y geografia.")
      print("2. Area de ciencias sociales y linguisticas.")
      print("3. Area de ciencias puras y aplicadas.")
      opn1_lib = input("Area:")
      while opn1_lib != "1" and opn1_lib != "2" and opn1_lib != "3":
        opn1_lib = input("Por favor, solo escriba '1', '2'o '3': ")
      if opn1_lib == "1":
        print("\nEliga un curso: geografia, historia o literatura")
        opnlib_1 = input("Curso:")
        while opnlib_1 != "geografia" and opnlib_1 != "historia" and opnlib_1 != "literatura":
          opnlib_1 = input("Por favor, solo escriba 'geografia', 'historia' o 'literatura'\n")
        if opnlib_1 == "geografia":
          print("\nEliga un libro: ")
          print("1. Geografia mundial contemporanea/Editorial Aique")
          print("2. Las ocho regiones naturales/Javier Pulgar Vidal")
          print("3. Atlas de Geografia del Mundo/GEOEDUCAR")
          opnlib_1_1 = input("Eliga un libro:")
          while opnlib_1_1 != "1" and opnlib_1_1 != "2" and opnlib_1_1 != "3":
            opnlib_1_1 = input("Por favor, solo escoja 1, 2 o 3\n")
          print("libro separado")
        elif opnlib_1 == "historia":
          print("\nEliga un libro: ")
          print("1. Diccionario de historia del mundo actual/Pilar Tobos Sanchez")
          print("2. Todo lo que hay que saber: una pequeña historia del mundo/Loel Zwecker")
          print("3. Comentarios Reales de los Incas/Inca Garcilaso de la Vega")
          print("4. Historia de la Corrupcion del Peru/Alfonso W. Quiroz")
          opnlib_1_2 = input("Eliga un libro:")
          while opnlib_1_2 != "1" and opnlib_1_2 != "2" and opnlib_1_2 != "3" and opnlib_1_2 != "4":
            opnlib_1_2 = input("Por favor, solo escoja 1, 2, 3 o 4\n")
          print("libro separado")
        elif opnlib_1 == "literatura":
          print("\nEliga un libro: ")
          print("1. La Divina Comedia/Dante Alighieri")
          print("2. Hamlet/William Shakespeare")
          print("3. Azul/Ruben Dario")
          print("4. Los Heraldos Negros/Cesar Vallejo")
          print("5. La Ciudad y los Perros/Mario Vargas Llosa")
          opnlib_1_3 = int(input("Eliga un libro:"))
          while opnlib_1_3 != 1 and opnlib_1_3 != 2 and opnlib_1_3 != 3 and opnlib_1_3 != 4 and opnlib_1_3 != 5:
            opnlib_1_3 = int(input("Por favor, solo escoja 1, 2, 3, 4 o 5\n"))
          if opnlib_1_3 == 1 or opnlib_1_3 == 2 or opnlib_1_3 == 3 or opnlib_1_3 == 4 or opnlib_1_3 == 5:
            print("libro separado")   
      elif opn1_lib == "2":
        opnlib_2 = input("Eliga un curso: economia, filosofia, psicologia, linguistica o politica\n")
        while opnlib_2 != "economia" and opnlib_2 != "filosofia" and opnlib_2 != "psicologia" and opnlib_2 != "politica" and opnlib_2 != "linguistica":
          opnlib_2 = input("Solo escriba 'economia', 'filosofia', 'psicologia', 'linguistica' o 'politica'\n")
        if opnlib_2 == "economia":
          print("\nEliga un libro:")
          print("1. La Riqueza de las Naciones/Adam Smith")
          print("2. Teoria General del Empleo, dek Interes y del Dinero/John Maynard Keynes")
          print("3. El Capital/Karl Marx")
          opnlib_2_1 = int(input("Eliga un libro:"))
          while opnlib_2_1 != 1 and opnlib_2_1 != 2 and opnlib_2_1 != 3:
            opnlib_2_1 = int(input("Por favor, solo escoja 1, 2 o 3\n"))
          if opnlib_2_1 == 1 or opnlib_2_1 == 2 or opnlib_2_1 == 3:
            print("libro separado")
        elif opnlib_2 == "filosofia":
          print("\nEliga un libro:")
          print("1. El Discurso del Metodo/Rene Descartes")
          print("2. Investigacion sobre el Entendimiento Humano/David Hume")
          print("3. Asi hablo Zaratustra/Friedrich Nietzsche")
          print("4. Historia de la Filosofia/Frederick Copleston")
          print("5. Apologia de Socrates/Platon")
          opnlib_2_2 = int(input("Eliga un libro:"))
          while opnlib_2_2 != 1 and opnlib_2_2 != 2 and opnlib_2_2 != 3 and opnlib_2_2 != 4 and opnlib_2_2 != 5:
            opnlib_2_2 = int(input("Por favor, solo escoja 1, 2, 3, 4 o 5\n"))
          if opnlib_2_2 == 1 or opnlib_2_2 == 2 or opnlib_2_2 == 3 or opnlib_2_2 == 4 or opnlib_2_2 == 5:
            print("libro separado")
        elif opnlib_2 == "psicologia":
          print("\nEliga un libro:")
          print("1. Inteligencia Emocional 2.0/Travis Bradberry & Jean Greaves")
          print("2. Inteligencias Multiples/Howard Gardner")
          print("3. Introduccion al Psicoanalisis/Sigmund Freud")
          print("4, Como funciona la mente/Steven Pinker")
          opnlib_2_3 = int(input("Eliga un libro:"))
          while opnlib_2_3 != 1 and opnlib_2_3 != 2 and opnlib_2_3 != 3 and opnlib_2_3 != 4:
            opnlib_2_3 = int(input("Por favor, solo escoja 1, 2, 3 o 4\n"))
          if opnlib_2_3 == 1 or opnlib_2_3 == 2 or opnlib_2_3 == 3 or opnlib_2_3 == 4:
            print("libro separado")
        elif opnlib_2 == "linguistica":
          print("\nEliga un libro:")
          print("1. Signos de contrabando. Informe contra la idea de comunicación/Antonio Valdecantos")
          print("2. Gramatica y ortografia basicas de la Lengua Espaniola/Real Academia Espaniola")    
          print("3. Lagartera. Bordados en el lenguaje *Palabrario, locuciones, dichos y consejas*/Ricardo Moreno Otero")
          opnlib_2_4 = int(input("Eliga un libro:"))
          while opnlib_2_4 != 1 and opnlib_2_4 != 2 and opnlib_2_4 != 3:
            opnlib_2_4 = int(input("Por favor, solo escoja 1, 2 o 3\n"))
          if opnlib_2_4 == 1 or opnlib_2_4 == 2 or opnlib_2_4 == 3:
            print("libro separado")
        elif opnlib_2 == "politica":
          print("\nEliga un libro:")
          print("1. El Principe/Nicolas Maquiavelo")
          print("2. Derecha e Izquierda/Norberto Bobbio")
          print("3. La Republica/Platon")
          opnlib_2_5 = int(input("Eliga un libro:"))
          while opnlib_2_5 != 1 and opnlib_2_5 != 2 and opnlib_2_5 != 3:
            opnlib_2_5 = int(input("Por favor, solo escoja 1, 2 o 3\n"))
          if opnlib_2_5 == 1 or opnlib_2_5 == 2 or opnlib_2_5 == 3:
            print("libro separado")
      elif opn1_lib == "3":
        opnlib_3 = input("Eliga un curso: matematica, fisica, quimica, biologia, medicina, contabilidad o ingenieria\n")
        while opnlib_3 != "matematica" and opnlib_3 != "fisica" and opnlib_3 != "quimica" and opnlib_3 != "biologia" and opnlib_3 != "medicina" and opnlib_3 != "contabilidad" and opnlib_3 != "ingenieria":
          opnlib_3 = input("Solo escriba 'matematica', 'fisica', 'quimica', 'biologia', 'medicina', 'contabilidad' o 'ingenieria'\n")
        if opnlib_3 == "matematica":
          print("\nEliga un libro:")
          print("1. Inteligencia Matematica/Eduardo Saenz de Cabezon")
          print("2. Geometria Analitica Vectorial/Lourdes Kala Bejar")
          print("3. Analisis Matematico 1. 2da Edicion/Armando Venero")
          print("4. Fundamentos de la Teoria de los Numeros/I. Vinogradov. Editorial Mir Moscu")
          opnlib_3_1 = int(input("Eliga un libro:"))
          while opnlib_3_1 != 1 and opnlib_3_1 != 2 and opnlib_3_1 != 3 and opnlib_3_1 != 4:
            opnlib_3_1 = int(input("Por favor, solo escoja 1, 2 o 3\n"))
          if opnlib_3_1 == 1 or opnlib_3_1 == 2 or opnlib_3_1 == 3 or opnlib_3_1 == 4:
            print("libro separado")
        elif opnlib_3 == "fisica":
          print("\nEliga un libro:")
          print("1. Fisica. 6ta edicion/Douglas C. Giancoli")
          print("2. Fundamentos de Fisica. 10ma edicion/Raymond A. Serway & Chris Vuille")
          print("3. Fisica General/Ignacio Martin Bragado")
          opnlib_3_2 = int(input("Eliga un libro:"))
          while opnlib_3_2 != 1 and opnlib_3_2 != 2 and opnlib_3_2 != 3:
            opnlib_3_2 = int(input("Por favor, solo escoja 1, 2 o 3\n"))
          if opnlib_3_2 == 1 or opnlib_3_2 == 2 or opnlib_3_2 == 3:
            print("libro separado")
        elif opnlib_3 == "quimica":
          print("\nEliga un libro:")
          print("1. Quimica. 7ma edicion/Raymond Chung")
          print("2. Quimica La Ciencia Central. 9na edicion/Editorial Pearson")
          print("3. La tabla Periodica de los Elementos Quimicos/Editorial CSIC")
          opnlib_3_3 = int(input("Eliga un libro:"))
          while opnlib_3_3 != 1 and opnlib_3_3 != 2 and opnlib_3_3 != 3:
            opnlib_3_3 = int(input("Por favor, solo escoja 1, 2 o 3\n"))
          if opnlib_3_3 == 1 or opnlib_3_3 == 2 or opnlib_3_3 == 3:
            print("libro separado")
        elif opnlib_3 == "biologia":
          print("\nEliga un libro:")
          print("1. Biología para Dummies/Rene Fester Kratz & Donna Rae Siegfried")
          print("2. La celula/Geoffrey M. Cooper & Robert E. Haussman")
          print("3. Introduccion a la Botanica/Murray W. Nabors")
          opnlib_3_4 = int(input("Eliga un libro:"))
          while opnlib_3_4 != 1 and opnlib_3_4 != 2 and opnlib_3_4 != 3:
            opnlib_3_4 = int(input("Por favor, solo escoja 1, 2 o 3\n"))
          if opnlib_3_4 == 1 or opnlib_3_4 == 2 or opnlib_3_4 == 3:
            print("libro separado")
        elif opnlib_3 == "medicina":
          print("\nEliga un libro:")
          print("1. La doble helice/James Dewey Watson")
          print("2. Anatomia Humana/Henri Rouviere & Andrer Delmas")
          print("3. Tratado de Fisiologia Medica/Arthur Guyton & John E. Hall")
          opnlib_3_5 = int(input("Eliga un libro:"))
          while opnlib_3_5 != 1 and opnlib_3_5 != 2 and opnlib_3_5 != 3:
            opnlib_3_5 = int(input("Por favor, solo escoja 1, 2 o 3\n"))
          if opnlib_3_5 == 1 or opnlib_3_5 == 2 or opnlib_3_5 == 3:
            print("libro separado")  
        elif opnlib_3 == "contabilidad":
          print("\nEliga un libro:")
          print("1. Diccionario contable y financiero/Jorge Gudino")
          print("2. Contabilidad Administrativa/David Noel Ramirez Padilla")
          print("3. Contabilidad/Horngren Charles T.")
          opnlib_3_6 = int(input("Eliga un libro:"))
          while opnlib_3_6 != 1 and opnlib_3_6 != 2 and opnlib_3_6 != 3:
            opnlib_3_6 = int(input("Por favor, solo escoja 1, 2 o 3\n"))
          if opnlib_3_6 == 1 or opnlib_3_6 == 2 or opnlib_3_6 == 3:
            print("libro separado")  
        elif opnlib_3 == "ingenieria":
          print("\nEliga un libro:")
          print("1. Teoria General de Sistemas/Ludwig Von Bertalanffy")
          print("2. Sistemas Operativos/Ann Mclver McHoes")
          print("3. Analisis y Disenio de Sistemas/Kendall y Kendall")
          opnlib_3_7 = int(input("Eliga un libro:"))
          while opnlib_3_7 != 1 and opnlib_3_7 != 2 and opnlib_3_7 != 3:
            opnlib_3_7 = int(input("Por favor, solo escoja 1, 2 o 3\n"))
          if opnlib_3_7 == 1 or opnlib_3_7 == 2 or opnlib_3_7 == 3:
            print("libro separado")
  elif opn == "2":
    cab_list = cabinas_disp()
    while True:
      print("\nCabinas disponibles:", cab_list)
      opn2_cab = input("Eliga una cabina: ")
      if int(opn2_cab) in cab_list:
        print("Cabina separada.")
        break
      else:
        print("Cabina no disponible.")
  elif opn == "3":
    print("\nEliga el motivo por el cual quieras separar el area audiovisual:")
    print("1. Conferencia")
    print("2. Presentacion de trabajo de tesis")
    print("3. Exposicion")
    motv = int(input("Motivo:"))
    while motv != 1 and motv != 2 and motv != 3:
      motv = int(input("Por favor, solo escoja 1, 2 o 3\n"))  
    if motv == 1:
      print("Area audiovisual para conferencia separada.") 
    elif motv == 2:
      print("Area audiovisual para presentacion de trabajo de tesis separada.")
    elif motv == 3:
      print("Area audiovisual para exposicion separada.") 
  elif opn == "4":
    input("\nEscriba su reclamo:\n")
    print("Reclamo enviado.")
  rep_all = input("\nElegir otro servicio?\n")
  while rep_all != "si" and rep_all != "no":
      rep_all = input("Solo escriba 'si' o 'no'\n")
  if rep_all == "no":
    print("\nGracias por utilizar el programa!")
    break